package de.lovelybooks.arch2017.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import de.lovelybooks.arch2017.Arch2017TestApplication;
import de.lovelybooks.arch2017.model.Author;
import de.lovelybooks.arch2017.service.AuthorService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Arch2017TestApplication.class)
@WebIntegrationTest({"server.port=0", "management.port=0"})
public class AuthorControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private AuthorService authorService;

    private MockMvc mockMvc;

    private List<Author> mockedAuthorList;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        Author mockedAuthor = new Author();
        mockedAuthor.setId(123L);
        mockedAuthor.setName("Horst");
        mockedAuthorList = new ArrayList<>();
        mockedAuthorList.add(mockedAuthor);
        Mockito.when(authorService.findAuthorsByName(Mockito.anyString())).thenReturn(mockedAuthorList);
    }

    @Test
    public void testFindByNames() throws Exception {
        String json = mockMvc.perform(get("/authors/byName/horst"))
                             .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        Assert.assertEquals(json, "[{\"id\":123,\"name\":\"Horst\",\"shortName\":\"Ho\"}]");
    }
}
