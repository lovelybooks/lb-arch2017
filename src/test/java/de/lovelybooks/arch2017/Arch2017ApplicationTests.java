package de.lovelybooks.arch2017;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Arch2017Application.class)
@WebAppConfiguration
public class Arch2017ApplicationTests {

    @Test
    public void contextLoads() {
    }

}
