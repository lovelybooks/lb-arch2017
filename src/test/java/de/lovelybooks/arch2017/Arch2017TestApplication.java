package de.lovelybooks.arch2017;

import org.mockito.Mockito;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import de.lovelybooks.arch2017.service.AuthorService;

@SpringBootApplication
public class Arch2017TestApplication {

    @Bean
    public AuthorService authorService() {
        return Mockito.mock(AuthorService.class);
    }


}
