package de.lovelybooks.arch2017.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import de.lovelybooks.arch2017.dto.AuthorRequestDTO;
import de.lovelybooks.arch2017.model.Author;

@Component
public class AuthorRequestValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Author.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "name.empty");
        AuthorRequestDTO author = (AuthorRequestDTO) target;
        if (author.getName().length() < 3) {
            errors.reject("name.tooShort");
        }
    }

}
