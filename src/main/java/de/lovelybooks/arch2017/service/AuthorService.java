package de.lovelybooks.arch2017.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.lovelybooks.arch2017.model.Author;
import de.lovelybooks.arch2017.repository.AuthorRepository;

@Service
@Transactional
public class AuthorService {

    @Autowired
    private AuthorRepository authorRepository;

    public Page<Author> findAll(Pageable pageable) {
        return authorRepository.findAll(pageable);
    }

    public List<Author> findAuthorsByName(String name) {
        return authorRepository.findByName(name);
    }

    public Author save(Author author) {
        return authorRepository.save(author);
    }
}
