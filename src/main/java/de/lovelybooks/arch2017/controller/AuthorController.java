package de.lovelybooks.arch2017.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.lovelybooks.arch2017.dto.AuthorRequestDTO;
import de.lovelybooks.arch2017.dto.AuthorResponseDTO;
import de.lovelybooks.arch2017.exception.BadRequestException;
import de.lovelybooks.arch2017.model.Author;
import de.lovelybooks.arch2017.service.AuthorService;
import de.lovelybooks.arch2017.validator.ValidationService;

@RestController
@RequestMapping(path = "authors", produces = "application/json")
public class AuthorController {

    @Autowired
    private AuthorService authorService;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private MessageSource messageSource;


    @RequestMapping(path = "/", method = GET)
    public Page<AuthorResponseDTO> findAllAuthors(Pageable pageable) {
        Page<Author> authorPage  = authorService.findAll(pageable);

        return new PageImpl<AuthorResponseDTO>(authorPage.getContent()
                                                         .stream()
                                                         .map(AuthorResponseDTO::new)
                                                         .collect(Collectors.toList()),
                                               pageable,
                                               authorPage.getTotalElements());
    }

    @RequestMapping(path = "/", method = POST)
    public AuthorResponseDTO addAuthor(@Valid @RequestBody AuthorRequestDTO dto,
                                       BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new BadRequestException(bindingResult.getFieldError().getDefaultMessage());
        }
        return new AuthorResponseDTO(authorService.save(dto.toEntity()));
    }

    @RequestMapping(path = "byName/{name}", method = GET)
    public List<AuthorResponseDTO> findAuthorsByName(@PathVariable String name,
                                                     Locale locale) {
        if (!validationService.checkIfValidName(name)) {
            throw new BadRequestException(messageSource.getMessage("error.params.invalid", null, locale));
        }
        List<AuthorResponseDTO> responseDTOs = new ArrayList<>();
        authorService.findAuthorsByName(name).forEach(author -> responseDTOs.add(new AuthorResponseDTO(author)));

        return responseDTOs;
    }

}
