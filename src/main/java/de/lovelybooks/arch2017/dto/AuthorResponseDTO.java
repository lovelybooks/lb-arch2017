package de.lovelybooks.arch2017.dto;

import de.lovelybooks.arch2017.model.Author;

public class AuthorResponseDTO extends Author {

    private String shortName;

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public AuthorResponseDTO(Author entity) {
        setId(entity.getId());
        setName(entity.getName());
        setShortName(entity.getName().substring(0, 2));
    }
}
