package de.lovelybooks.arch2017.dto;

import javax.validation.constraints.Size;

import de.lovelybooks.arch2017.model.Author;

public class AuthorRequestDTO {

    @Size(min = 3, max = 200, message = "{author.name.size}")
    private String name;

    public AuthorRequestDTO() {
    }

    public AuthorRequestDTO(Author entity) {
        setName(entity.getName());
    }

    public Author toEntity() {
        Author author = new Author();
        author.setName(this.name);
        return author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
