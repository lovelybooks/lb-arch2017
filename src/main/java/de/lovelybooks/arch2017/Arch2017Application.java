package de.lovelybooks.arch2017;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Arch2017Application {

    public static void main(String[] args) {
        SpringApplication.run(Arch2017Application.class, args);
    }

}
