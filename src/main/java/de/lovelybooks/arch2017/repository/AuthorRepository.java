package de.lovelybooks.arch2017.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import de.lovelybooks.arch2017.model.Author;

public interface AuthorRepository extends PagingAndSortingRepository<Author, Long> {

    List<Author> findByName(String name);
}
