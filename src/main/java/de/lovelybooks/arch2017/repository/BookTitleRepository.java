package de.lovelybooks.arch2017.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import de.lovelybooks.arch2017.model.BookTitle;

public interface BookTitleRepository extends PagingAndSortingRepository<BookTitle, Long> {

}
